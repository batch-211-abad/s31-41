/*
	user{
	
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments:[
		{
			id - document identifier
			courseId - the unique identifier
			courseName - optional
			isPaid,
			dateEnrolled
		}
	]
}

*/
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required:[true,"Last Name is required"]
	},
	email : {
		type: String,
		required: [true, "Email is required"]
	},	
	password : {
		type: String,
		required: [true, "Password is required"]
	},	

	isAdmin : {
		type: Boolean,
		default: false
	},

	mobileNo : {
		type: String,
		default: [true, "Mobile Number is required"]
	},
	enrollments : [
		{
			courseId:{
				type: String,
				required: [true,"Course Id is required"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "Enrolled"
			},
		}
	]
})

module.exports = mongoose.model("User", userSchema);