const Course = require("../models/Course");

//Mini Activity
//Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

//reqBody refers to the input fields 
module.exports.addCourse = (reqBody) => {

	if (reqBody.isAdmin == true) {
		
		let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	}
	else {

		return false;

	}

	



	return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

};




//Solution 2


module.exports.addCourse = (data) => {

	if (data.isAdmin) {
		
		let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	}
	else {

		return false;

	}

	



	return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

};


//Retrieve all Courses
/*
	1. Retrieve all the courses from the database
		Model.find({})
*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	});
};



//Retrieve all ACTIVE courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true.
*/


module.exports.getAllActive = () =>{
	return Course.find({isActive : true}).then(result=>{
		return result;
	});
};


//Retrieve a SPECIFIC Course

/*
	1. Retrieve the course that matches the course ID provided from the URL.
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};

// UPDATE a course

/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body. 

	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body. 
*/

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqbody.name,
		description: reqbody.description,
		price: reqbody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		};
	});
};



// ARCHIVING a course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		};
	});
};

